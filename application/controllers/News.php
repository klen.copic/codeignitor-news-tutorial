<?php
class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('news_model');
		$this->load->helper('url_helper');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');

		$data['title'] = "Create News";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/create');
        	$this->load->view('templates/footer');
		}else{
			$this->news_model->set_news();
			$this->load->view('news/success');
		}
	}

	public function view($slug)
	{
		
		$data['news_item'] = $this->news_model->get_news_where($slug);
	
		$data['title'] = "News Item";
	

		$this->load->view('templates/header', $data);
        $this->load->view('news/view', $data);
        $this->load->view('templates/footer', $data);

	}


    public function index()
	{
		
		$data['news'] = $this->news_model->get_news();
		$data['title'] = "News Archive";

		$this->load->view('templates/header', $data);
        $this->load->view('news/index', $data);
        $this->load->view('templates/footer', $data);

	}

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}


}